#!/usr/bin/env iocsh.bash

# -----------------------------------------------------------------------------
# EPICS - DataBase
# -----------------------------------------------------------------------------
# Burster Presicion Resistance Decade 1427
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
require(ethercatmc)

# -----------------------------------------------------------------------------
# E3 common module
# -----------------------------------------------------------------------------
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")
epicsEnvSet("IPADDR",        "$(SM_IPADDR=172.30.242.18)")
epicsEnvSet("IPPORT",        "$(SM_IPPORT=5000)")
epicsEnvSet("ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("PREFIX",        "$(SM_PREFIX=LabS-ESSIIP:)")
epicsEnvSet("P",             "$(SM_PREFIX=LabS-ESSIIP:)")
epicsEnvSet("EGU",           "$(SM_EGU=mm)")
epicsEnvSet("PREC",          "$(SM_PREC=3)")

# -----------------------------------------------------------------------------
# EtherCAT MC Controller
# -----------------------------------------------------------------------------
iocshLoad("$(ethercatmc_DIR)EthercatMCController.iocsh")


# -----------------------------------------------------------------------------
# Axis 1 configuration and instantiation
# -----------------------------------------------------------------------------
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=MC-MCU-02:m1)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=1)")
epicsEnvSet("DESC",          "$(SM_DESC=Lower=Right)")
epicsEnvSet("AXISCONFIG",    "HomProc=1;HomPos=0;encoder=ADSPORT=501/.ADR.16#3040010,16#80000049,2,2")

iocshLoad("$(ethercatmc_DIR)EthercatMCAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)EthercatMCAxisdebug.iocsh")
iocshLoad("$(ethercatmc_DIR)EthercatMCAxishome.iocsh")

# -----------------------------------------------------------------------------
# Axis 2 configuration and instantiation
# -----------------------------------------------------------------------------
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=MC-MCU-02:m2)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=2)")
epicsEnvSet("DESC",          "$(SM_DESC=Upper=Left)")
epicsEnvSet("AXISCONFIG",    "HomProc=1;HomPos=0;encoder=ADSPORT=501/.ADR.16#3040010,16#8000004F,2,2")

iocshLoad("$(ethercatmc_DIR)EthercatMCAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)EthercatMCAxisdebug.iocsh")
iocshLoad("$(ethercatmc_DIR)EthercatMCAxishome.iocsh")

iocInit()
